<?php


/**
 * The form of the removal of Nodes.
 */
function _cleaning_node_form($form_state) {
  $form = array();
  
  $form['filter'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Filters'),
    '#attributes' => array(
      'class' => 'cleaning_filters',
    ),
  );
  
  $form['filter']['publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#description' => t('If set, will only delete the published material.'),
    '#default_value' => 1,
  );
  
  $types = node_get_types();  
  $options = array();
  foreach ($types as $id => $type) {
    $options[$id] = $type->name;
  }
  
  $form['filter']['type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => $options,
  );
  
  $min_data = db_result(db_query("SELECT MIN(created) FROM {node}"), 0);
  $form['filter']['date_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From date'),
    '#default_value' => date('Y-m-d H:i:s', $min_data),  //2012-12-31 23:59:59
    '#size' => 22,
    '#maxlength' => 32,
  );

  $form['filter']['date_to'] = array(
    '#type' => 'textfield',
    '#title' => t('To date'),
    '#default_value' => date('Y-m-d H:i:s'),  //2012-12-31 23:59:59
    '#size' => 22,
    '#maxlength' => 32,
  );
  
  $max_uid = db_result(db_query("SELECT MAX(uid) FROM {users}"), 0);
  $form['filter']['user_text'] = array(
    '#value' => '<div>'. t('Contents with User ID=1 is not removed by this module.') .'</div>',
  );
  
  $form['filter']['user_from'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID from'),
    '#default_value' => '2',
    '#size' => 8,
    '#maxlength' => 32,
    '#description' => t('Contents with User ID=1 is not removed by this module.') .' '.
      t('The minimum value of 0, maximum !max_uid. If set to 0, this means removing the material created a guest.', array(
      '!max_uid' => $max_uid,
    )),
  );
  
  $form['filter']['user_to'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID to'),
    '#default_value' => $max_uid,
    '#size' => 8,
    '#maxlength' => 32,
    '#description' => t('The minimum value of 0, maximum !max_uid.', array(
      '!max_uid' => $max_uid,
    )),
  );
  
  $form['description'] = array(
    '#type' => 'fieldset',
    '#title' => t('Description'),
  );
  
  $form['description']['text'] = array(
    '#value' => '<div id="cleaning_description">'.
      t('The description will be generated when you unlock button startup procedure.')
      .'</div>',
  );
  
  $form['start'] = array(
    '#type' => 'fieldset',
    '#title' => t('Starting'),
  );
  
  $form['start']['accepts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unlock button'),
    '#description' => t('When you delete the data can not be restored.'),
    '#ahah' => array(
      'path' => 'admin/content/cleaning_js',
      'wrapper' => 'cleaning_description',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  $form['start']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array(
      'disabled' => 'disabled',
      'class' => 'cleaning_submit',
    ),
  );
  
  return $form;
}


/**
 * Validate filter parameters.
 */
function _cleaning_node_form_validate($form, &$form_state) {
  $user_from = (int)$form_state['values']['user_from'];
  $user_to = (int)$form_state['values']['user_to'];
  if ($user_from == 1) {
    $user_from = 2;
  }
  if ($user_from < 0) {
    form_set_error('user_from', t('The minimum value of 0.'));
  }
  elseif ($user_to < $user_from) {
    form_set_error('user_from', t('Incorrectly specified boundaries select user IDs.'));
    form_set_error('user_to', t('Incorrectly specified boundaries select user IDs.'));
  }
  
  $data_from = strtotime($form_state['values']['date_from']);
  $data_to = strtotime($form_state['values']['date_to']);
  if (date('Y-m-d H:i:s', $data_from) != $form_state['values']['date_from']) {
    form_set_error('date_from', t('Invalid date format.'));
  }
  if (date('Y-m-d H:i:s', $data_to) != $form_state['values']['date_to']) {
    form_set_error('date_to', t('Invalid date format.'));
  }
  if ($data_to < $data_from) {
    form_set_error('date_to', t('Incorrectly specified period of time.'));
    form_set_error('date_from', t('Incorrectly specified period of time.'));
  }
}


/**
 * Create Batch process for delete Nodes.
 */
function _cleaning_node_form_submit($form, &$form_state) {
  $operations = array();
  
  $result_filter = _cleaning_filter_node_submit($form_state['values']);
  $a_where = _cleaning_sql_node_where($result_filter);
  $result = db_query("SELECT nid FROM {node} WHERE ". $a_where['sql'], $a_where['args']);
  $count = 0;
  $a_nodes = array();
  while ($node = db_fetch_object($result)) {
    if ($count++ > 9) {
      $operations[] = array('cleaning_nodes_batch', array($a_nodes));
      $a_nodes = array();
      $count = 0;
    }
    $a_nodes[] = $node->nid;
  }
  
  if (count($a_nodes)) {
    $operations[] = array('cleaning_nodes_batch', array($a_nodes));
  }
  
  $batch = array(
    'operations' => $operations,
    'title' => 'Delete nodes',
    'finished' => 'cleaning_batch_finished',
  );
  batch_set($batch);  //Start batch delete nodes
}


/**
 * Generated description for process.
 * Validate form.
 */
function _cleaning_generated_description_js() {
  $out_data = array(
    'error' => array(),
    'description' => '',
    'result' => '',
  );
  
  //Load Form
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $form['#post'] = $_POST;
  
  //Validate form
  switch ($form['#post']['form_id']) {
    case '_cleaning_node_form':
      $result_filter = _cleaning_filter_node_submit($form['#post']);

      if ($result_filter['user_from'] < 0) {
        $out_data['error'][] = 'Error field "'. t('User ID from')
          .'" - '. t('The minimum value of 0.');
      }
      elseif ($result_filter['user_to'] < $result_filter['user_from']) {
        $out_data['error'][] = 'Error fields "'. t('User ID from') .'", "'.
          t('User ID to') .'" - '. t('Incorrectly specified boundaries select user IDs.');
      }
      
      if (date('Y-m-d H:i:s', $result_filter['date_from']) != $form['#post']['date_from']) {
        $out_data['error'][] = 'Error field "'. t('From date')
          .'" - '. t('Invalid date format.');
      }
      if (date('Y-m-d H:i:s', $result_filter['date_to']) != $form['#post']['date_to']) {
        $out_data['error'][] = 'Error field "'. t('Date to')
          .'" - '. t('Invalid date format.');
      }
      if ($result_filter['date_to'] < $result_filter['date_from']) {
        $out_data['error'][] = 'Error fields "'. t('From date') .'", "'.
          t('Date to') .'" - '. t('Incorrectly specified period of time.');
      }
      break;
  }
  
  if (!count($out_data['error'])) {
    //Generated description
    switch ($form['#post']['form_id']) {
      case '_cleaning_node_form':
        if ($result_filter['node_types_count']) {
          $node_type = format_plural(count($form['#post']['type']),
            'type materials "%types"',
            'types materials "%types"',
            array('%types' => $result_filter['node_types'], )
          );
        }
        else {
          $node_type = '';
        }
        
        if ($result_filter['user_to'] == $result_filter['user_from']) {
          $result_filter['user_to'] = 1;
        }
        
        $out_data['description'] = format_plural($user_to,
          'Delete node !node-type in the status "!publish" with the date of "!date-from" to "!date-to", create a user ID of "!uid-from".',
          'Delete node !node-type in the status "!publish" with the date of "!date-from" to "!date-to", create a users ID of "!uid-from" to "!uid-to" (excluding administrator UID=1).',
          array(
            '!node-type' => $node_type,
            '!publish' => ($result_filter['publish'] ? t('Published') : t('Not published')),
            '!date-from' => $form['#post']['date_from'],
            '!date-to' => $form['#post']['date_to'],
            '!uid-from' => $result_filter['user_from'],
            '!uid-to' => $result_filter['user_to'],
          )
        );
        
        $a_where = _cleaning_sql_node_where($result_filter);
        $out_data['result'] = t('Found !count records.', array(
          '!count' => db_result(db_query(
            "SELECT COUNT(*) FROM {node} WHERE ". $a_where['sql'], $a_where['args']
          ))
        ));
        break;
    }
  }

  drupal_json(array(
    'status' => TRUE,
    'data' => theme('cleaning_generated_description', $out_data)
  ));
}


/*
 * Load submit data to array.
 */
function _cleaning_filter_node_submit($values) {
  $result_filter = array(
    'user_from' => (int)$values['user_from'],
    'user_to' => (int)$values['user_to'],
    'date_from' => strtotime($values['date_from']),
    'date_to' => strtotime($values['date_to']),
    'publish' => (isset($values['publish'])
        && (int)$values['publish'] ? 1 : 0),
    'node_types' => '',
    'node_types_count' => 0,
    'a_node_types' => array(),
  );
  
  if (isset($values['type']) && count($values['type'])) {
    $values['type'] = array_diff($values['type'], array(0));
    $result_filter['node_types'] = implode(', ', $values['type']);
    $result_filter['a_node_types'] = $values['type'];
    $result_filter['node_types_count'] = count($values['type']);
  }
  if ($result_filter['user_from'] == 1) {
    $result_filter['user_from'] = 2;
  }
  return $result_filter;
}


/**
 * Build SQL - WHERE part.
 */
function _cleaning_sql_node_where($result_filter) {
  $where = array(
    'sql' => '',
    'args' => array(),
  );
  
  $where['sql'] = ' status = %d AND (created >= %d AND created <= %d)';
  $where['args'][] = $result_filter['publish'];
  $where['args'][] = $result_filter['date_from'];
  $where['args'][] = $result_filter['date_to'];
  
  if ($result_filter['user_to'] != 1) {
    $where['sql'] .= ' AND';
    if ($result_filter['user_from'] == 0) {
      $where['sql'] .= ' (';
    }
    $where['sql'] .= ' (uid >= %d AND uid <= %d)';
    if ($result_filter['user_from'] == 0) {
      $where['sql'] .= ' OR (uid = %d))';
      $where['args'][] = 2;
      $where['args'][] = $result_filter['user_to'];
      $where['args'][] = 0;
    }
    else {
      $where['args'][] = $result_filter['user_from'];
      $where['args'][] = $result_filter['user_to'];
    }
  }
  else {
    $where['sql'] .= ' AND uid = %d';
    $where['args'][] = $result_filter['user_from'];
  }
  
  if ($result_filter['node_types_count']) {
    $where['sql'] .= ' AND type IN('. db_placeholders($result_filter['a_node_types'], 'text') .')';
    $where['args'] = array_merge($where['args'], array_values($result_filter['a_node_types']));
  }
  
  return $where;
}


/**
 * Theming AJAX generated description.
 */
function theme_cleaning_generated_description($out_data) {
  $ouput = '';
  
  if (count($out_data['error'])) {
    foreach ($out_data['error'] as $error) {
      $output .= '<p>'. $error .'</p>';
    }
    $output = '<div class="cleaning_desc_error">'. $output ."</div>\n";
  }
  
  $output .= '<div class="cleaning_desc_desc">'. $out_data['description'] ."</div>\n";
  $output .= '<div class="cleaning_desc_result">'. $out_data['result'] ."</div>\n";

  return '<div class="cleaning_desc_generate">'. $output ."</div>\n";
}








